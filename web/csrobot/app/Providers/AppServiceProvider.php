<?php

namespace App\Providers;

use App\Notification;
use App\Page;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('*', function ($view) {
            if (auth()->check()) {
                $currentUser = auth()->user();
                $currentBusiness = $currentUser->business;
                $businessSetting = $currentBusiness->setting;
                View::share('currentUser', $currentUser);
                View::share('currentBusiness', $currentBusiness);
                View::share('businessSetting', $businessSetting);
				
				if($currentUser->mainUser){
                $notifications = Notification::where(function($query) use($currentUser) {
                    $query->where('target', 'LIKE', '%:' . $currentUser->business_id . ':%')->orWhere('target', "all");
                    })->where('startDate', '<=', date("Y-m-d"))->where(function($query) {
                        $query->where('endDate', '>=', date("Y-m-d"))->orWhere('endDate', null);
                })->where(function($query)use($currentUser) {
                        $query->where('read', 'NOT LIKE',  '%:' . $currentUser->business_id . ':%')
						->orWhere('read', null);
                    })->orderBy('startDate','desc')->orderBy('created_at','desc')->limit(5)->get();

                $notificationCount = Notification::where(function($query)use($currentUser) {
                    $query->where('target', 'LIKE', '%:' . $currentUser->business_id . ':%')->orWhere('target', "all");
                })->where('startDate', '<=', date("Y-m-d"))->where(function($query) {
                        $query->where('endDate', '>=', date("Y-m-d"))->orWhere('endDate', null);
                })->where(function($query)use($currentUser) {
                        $query->where('read', 'NOT LIKE',  '%:' . $currentUser->business_id . ':%')
						->orWhere('read', null);
                    })->count();
				}
				else{
					$notifications = Notification::where(function($query) use($currentUser) {
                    $query->where('target', 'LIKE', '%:' . $currentUser->business_id . ':%')->orWhere('target', "all");
                    })->where('startDate', '<=', date("Y-m-d"))->where(function($query) {
						$query->where('endDate', '>=', date("Y-m-d"))->orWhere('endDate', null);
                })->where(function($query)use($currentUser) {
                        $query->where('read', 'NOT LIKE',  '%:' . $currentUser->business_id . ':%')
						->orWhere('read', null);
                    })->where('onlyMain', false)
					->orderBy('startDate','desc')->orderBy('created_at','desc')->limit(5)->get();

                $notificationCount = Notification::where(function($query)use($currentUser) {
                    $query->where('target', 'LIKE', '%:' . $currentUser->business_id . ':%')->orWhere('target', "all");
                })->where('startDate', '<=', date("Y-m-d"))->where(function($query) {
                    $query->where('endDate', '>=', date("Y-m-d"))->orWhere('endDate', null);
                })->where(function($query) use($currentUser){
                        $query->where('read', 'NOT LIKE',  '%:' . $currentUser->business_id . ':%')
						->orWhere('read', null);
                    })->where('onlyMain', false)->count();
				}

                View::share('notifications', $notifications);
                View::share('notificationCount', $notificationCount);

                $userPrivileges = collect($currentUser->privileges->pluck('id'));
                $appPages = Page::where(function ($query) use ($userPrivileges) {
                    $query->whereIn("privilege_id", $userPrivileges)
                        ->orWhere('privilege_id', null);
                })->get();
                View::share('appPages', $appPages);
            }
        });

        Blade::directive('moneyFormat', function ($money) {
            return "<?php echo number_format($money, 2); ?>";
        });
    }
}
