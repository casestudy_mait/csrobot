<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;

class BroadcastEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $title;
    public $message;
    public $event;
    public $action;

    public function __construct($action,$event,$title=null, $message=null)
    {
        $this->title = $title;
        $this->message = $message;
        $this->event = $event;
        $this->action = $action;
    }

    public function broadcastOn()
    {
        return new PrivateChannel("aibiz-channel");
    }

    public function broadcastAs()
    {
        return $this->event;
    }
}
