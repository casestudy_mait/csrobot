<?php

namespace App\Http\Controllers;


use App\Events\BroadcastEvent;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class AppController extends Controller
{


    public
    function index()
    {
        return response()
            ->view('index');

    }


    public
    function capture(Request $request)
    {

        $validator = validator($request->all(), [
            'x' => 'required|numeric',
            'y' => 'required|numeric',
        ]);


        $responseBody = (object)'';
        if ($validator->fails()) {
            $responseBody->statusCode = 96;
            $responseBody->content = $validator->errors()->first();
            return json_encode($responseBody, JSON_NUMERIC_CHECK | JSON_HEX_APOS);
        }

           // try{
            $x = $request->x;
            $y = $request->y;

            $message =    array( "x" => $x,"y" =>$y);
            broadcast(new BroadcastEvent('robot',"position-event", "xy", $message));


        return response()->json();

    }

}
