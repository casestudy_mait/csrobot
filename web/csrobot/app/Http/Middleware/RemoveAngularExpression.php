<?php

namespace App\Http\Middleware;

use Closure;

class RemoveAngularExpression
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */



    public function handle($request, Closure $next)
    {
        $removeCurlyBrackets = function($input){

            if(is_string ($input)){
                $input = preg_replace('/\{\{|\}\}/m', '', $input);
            }
            return $input;
        };

        $input = $request->all();
        $request->merge(array_map($removeCurlyBrackets, $input));

        return $next($request);
    }


}
