<?php

namespace App\Http\Middleware;

use App\enums\NotifyMsgTypes;
use Closure;

class CheckPrivilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()===null){
            return redirect('/logout');
        }

        $actions = $request->route()->getAction();
        $privileges = isset($actions['privileges']) ? $actions['privileges'] : null;

        if ($request->user()->hasAnyPrivilege($privileges) || !$privileges){
            return $next($request);
        }

        $notify = (object)'';
        $notify->type = NotifyMsgTypes::ERROR;
        $notify->text = "Access is Denied";
        session()->flash("notify", $notify);

        return redirect('/');
    }
}
