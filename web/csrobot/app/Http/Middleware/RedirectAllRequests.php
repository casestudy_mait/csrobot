<?php

namespace App\Http\Middleware;

use Closure;

class RedirectAllRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */



    public function handle($request, Closure $next)
    {
        return redirect()->away('https://www.aibizglobal.com/'.$request->path());
    }


}
