<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Robot Control | CS 17</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link rel="icon" type="image/x-icon" href="/favicon.ico"/>
    <link rel="stylesheet" href="/css/plugins.css?ab=ced"/>
    <link rel="stylesheet" href="/fonts/font_awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/css/theme.css"/>
    <link rel="stylesheet" href="/css/main.css?wop=1as">
    <script src="/js/plugins.js?ab=ce"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js?awe=67"></script>


</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="container-fluid pt-2  pt-md-4 page-body faded_white_light">
    <div class="container-fluid  p-md-3 ">
        <div class="row">
            <div class="center-x shadow px-md-4" id="term_of_use_div">
                <div class="mt-md-2 p-2">
                    <canvas id="robotCanvas" width="900" height="700" style="border:1px solid #d3d3d3;"/>
                </div>

            </div>
        </div>

    </div>
</div>

<script>

    function getX(x) {
        return Math.round(860 * x / 120);
    }

    function getY(y) {
        return 660 - Math.round(6.6 * y);
    }

    const canvas = document.getElementById('robotCanvas');
    const ctx = canvas.getContext('2d');
    var gradient = ctx.createLinearGradient(0, 0, 170, 0);
    gradient.addColorStop("0", "magenta");
    gradient.addColorStop("0.5" ,"blue");
    gradient.addColorStop("1.0", "red");

    // Fill with gradient
    ctx.strokeStyle = gradient;
    const pointsArray = [];
    redrawCanvas();

    function redrawCanvas() {

        ctx.clearRect(0,0,canvas.width, canvas.height);

        const img1 = new Image();
        img1.src = "/img/pos1.png";
        img1.onload = function () {
            ctx.drawImage(img1, getX(7), getY(22));
        };

        const img2 = new Image();
        img2.src = "/img/pos2.png";
        img2.onload = function () {
            ctx.drawImage(img2, getX(98), getY(22));
        };

        const img3 = new Image();
        img3.src = "/img/pos3.png";
        img3.onload = function () {
            ctx.drawImage(img3, getX(98), getY(62));
        };

        const img4 = new Image();
        img4.src = "/img/pos4.png";
        img4.onload = function () {
            ctx.drawImage(img4, getX(53), getY(62));
        };
        const img5 = new Image();
        img5.src = "/img/pos5.png";
        img5.onload = function () {
            ctx.drawImage(img5, getX(53), getY(92));
        };

        const img6 = new Image();
        img6.src = "/img/pos6.png";
        img6.onload = function () {
            ctx.drawImage(img6, getX(98), getY(92));
        };

        const img7 = new Image();
        img7.src = "/img/pos7.png";
        img7.onload = function () {
            ctx.drawImage(img7, getX(7), getY(92));
        };

        const img9 = new Image();
        img9.src = "/img/pos9.png";
        img9.onload = function () {
            ctx.drawImage(img9, getX(53), getY(22));
        };

        ctx.fillRect(getX(85), getY(35), getX(10), 5);
        ctx.fillRect(getX(30), getY(35), getX(20), 5);
        ctx.fillRect(getX(80), getY(70), getX(20), 5);
        ctx.fillRect(getX(30), getY(70), getX(10), 5);

        setTimeout(f => {
            if (pointsArray.length > 0) {

                ctx.beginPath();
                ctx.moveTo(getX(pointsArray[0].x), getY(pointsArray[0].y));

                for(var i = 1; i< pointsArray.length; i++){
                    ctx.lineTo(getX(pointsArray[i].x), getY(pointsArray[i].y));
                    ctx.stroke();
                }
                const len = pointsArray.length;
                if(len>0) {
                    let x = pointsArray[len - 1].x;
                    let y = pointsArray[len - 1].y;
                    let carLink = "/img/carxf.png";

                    if (len > 1) {
                        let px = pointsArray[len - 2].x;
                        let py = pointsArray[len - 2].y;

                        if (y === py && x !== px) {
                            carLink = x > px ? "/img/carxf.png" : "/img/carxb.png"
                        } else if (x === px && y !== py) {
                            carLink = y > py ? "/img/caryu.png" : "/img/caryd.png"
                        }
                    }

                    const carImg = new Image();
                    carImg.src = carLink;
                    carImg.onload = function () {
                        ctx.drawImage(carImg, getX(x-1),
                            getY(y+1));
                    };
                }
            }

        }, 200)
    }

    var pusher = new Pusher('eb39aef2b18d7225c251', {
        cluster: 'mt1',
        authEndpoint: '/channels/auth',
        auth: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }
    });

    var channel = pusher.subscribe('private-aibiz-channel');

    channel.bind('position-event', function (data) {
        const point = {};
        point.x = Number(data.message.x);
        point.y = Number(data.message.y);
        pointsArray.push(point);
        redrawCanvas();
    });

</script>
</body>
</html>
<?php /**PATH C:\Users\ccndu\Documents\Projects\Laravel\robot\resources\views/index.blade.php ENDPATH**/ ?>