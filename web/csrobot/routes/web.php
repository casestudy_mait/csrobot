<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;


Route::get('/', 'AppController@index');
Route::any('/positions', 'AppController@capture');

Route::post('/channels/auth', function (Request $request) {
    $stringToSign = $request->socket_id . ':' . $request->channel_name;
    $secret = env('PUSHER_APP_SECRET');
    $sig = hash_hmac('sha256', $stringToSign, $secret);
    return response()->json([
        'auth' => env('PUSHER_APP_KEY') . ':' . $sig
    ]);
});
