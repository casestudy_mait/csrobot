#!/usr/bin/env python

#####
# USING MCL TO DETECT ROBOT POSITION
# This is a program for ROS environment that is designed to 
# work alongside arduino code to detect robot position in a map.
# #### MAIT CASE STUDY ####
####

import math
import numpy as np
import bisect
import requests
import RPi.GPIO as GPIO
import rospy
from case_study.msg import CsData

MAP_WIDTH = 120
MAP_HEIGHT = 100
MAX_DISTANCE = math.hypot(120, 100)
NUM_PARTICLES = 5000

SENSOR_FROM_COG = 12 #Distance from sensor to center of gravity is 12 cm

import time
start_time = time.time()


#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24

#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

def tan(deg):
    if deg == 90 or deg == 270:
        return None
    return math.tan(math.radians(deg))


def atan(val):
    return round(math.degrees(math.atan(val)))


def weightGaussianKernel(robotDistance, particleDistance, std=0.7):
    euclideanDistance = np.linalg.norm(np.asarray(robotDistance) - np.asarray(particleDistance))
    #print(euclideanDistance,"eu",robotDistance,particleDistance,1 / np.sqrt(2 * np.pi) * np.exp(-euclideanDistance ** 2 /  (2 * std)))
    return 1 / np.sqrt(2 * np.pi) * np.exp(-euclideanDistance ** 2 /  (2 * std))

def resampling(particles):
    distribution = WeightedDistribution(particles)
    newParticles = list()

    for i in range(len(particles)):
        particle = distribution.random_select()
        if particle is None:
            x = np.random.randint(0, MAP_WIDTH)
            y = np.random.randint(0, MAP_HEIGHT)
            orient = np.random.randint(0, 360)
            newParticles.append(Particle(Point(x, y), orient, map))

        else:
            newParticles.append(Particle(particle.point, particle.angle, particle.map,particle.weight))

    return newParticles


def selectOne(particles):
    distribution = WeightedDistribution(particles)

    return  distribution.random_select()

#


class WeightedDistribution:

    def __init__(self, particles):

        accum = 0.0
        self.particles = particles
        self.distribution = list()
        for particle in self.particles:
            accum += particle.weight
            self.distribution.append(accum)

    def random_select(self):

        try:
            return self.particles[bisect.bisect_left(self.distribution, np.random.uniform(0, 1))]
        except IndexError:
            # When all particles have weights zero
            return self.particles[0]


class Map:
     # The ROBOT map. It is made up of fence and landmarks
    def __init__(self, landmarks, fence):
        self.landmarks = landmarks
        self.fence = fence


class Particle:
    # The MCL particle with point, angle and weight
    def __init__(self, point, angle, map,weight=0):
        self.point = point
        self.angle = angle
        self.map = map
        self.weight = weight

    def virtualStart(self):
        slope = tan(self.angle)
        if slope is None:
            if self.angle == 90:
                pointy = self.point.y - MAX_DISTANCE
            else:
                pointy = self.point.y + MAX_DISTANCE

            return Point(self.point.x, pointy)

        intercept = self.point.y - slope * self.point.x
        if self.angle > 360:
            angle = self.angle - 360
        else:
            angle = self.angle

        if angle < 90 or angle > 270:
            pointx = self.point.x - MAX_DISTANCE
        else:
            pointx = self.point.x + MAX_DISTANCE

        pointy = slope * pointx + intercept
        return Point(pointx, pointy)

    def virtualLine(self):
        slope = tan(self.angle)
        if slope is None:
            point1y = self.point.y - MAX_DISTANCE
            point2y = self.point.y + MAX_DISTANCE
            point1 = Point(self.point.x, point1y)
            point2 = Point(self.point.x, point2y)
            return Line(point1, point2)

        intercept = self.point.y - slope * self.point.x
        point1x = self.point.x - MAX_DISTANCE
        point2x = self.point.x + MAX_DISTANCE
        point1y = slope * point1x + intercept
        point2y = slope * point2x + intercept

        point1 = Point(point1x, point1y)
        point2 = Point(point2x, point2y)
        return Line(point1, point2)

    def distanceToLine(self, line):
        # print(line)
        if line.angle() == self.virtualLine().angle():
            return None

        if self.virtualLine().slope() is None:
            x = self.point.x
        elif line.slope() is None:
            x = line.point1.x
        else:
            x = (line.intercept() - self.virtualLine().intercept()) / (self.virtualLine().slope() - line.slope())

        if (x < line.point1.x and x < line.point2.x) or (x > line.point1.x and x > line.point2.x):
            return None

        if line.slope() is None:
            y = self.virtualLine().slope() * x + self.virtualLine().intercept()
        else:
            y = line.slope() * x + line.intercept()

        if (y < line.point1.y and y < line.point2.x) or (y > line.point1.y and y > line.point2.y):
            return None
        # print("x: ",x," y: ",y)
        return self.point.distanceToPoint(Point(round(x), round(y)))

    def distanceToLandmark(self, landmark):
        minDistance = None
        for line in landmark.getLines():
            distance = self.distanceToLine(line)
            if distance is not None and (minDistance is None or distance < minDistance):
                minDistance = distance

        return minDistance

    def distanceToFence(self):
        minDistance = None
        for line in self.map.fence.getLines():
            distance = self.distanceToLine(line)
            if distance is not None and distance > 0 and (minDistance is None or distance < minDistance):
                minDistance = distance

        return minDistance

    def distanceToLandmarkOrFence(self):
        minDistance = None
        for landmark in self.map.landmarks:
            distance = self.distanceToLandmark(landmark)
            if distance is not None and (minDistance is None or distance < minDistance):
                minDistance = distance

        distance = self.distanceToFence()
        if distance is not None and (minDistance is None or distance < minDistance):
            minDistance = distance

        return minDistance

    def __str__(self):
        virtualStart = self.virtualStart()
        return str(self.point.x)+","+str(self.point.y)+","+str(self.angle)+","+str(virtualStart.x)+","+str(virtualStart.y)



class Point:
    # Particle point with x and y position in the map
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def distanceToPoint(self, point):
        return math.hypot(self.x - point.x, self.y - point.y)

    def __str__(self):
        return "x: " + str(self.x) + ", y: " + str(self.y)


class Landmark:
    #Represent a landmark within the map using points and lines
    # that defines its shape and location
    def __init__(self, points):
        self.points = points

    def getLines(self):
        lines = list()
        for i in range(0, len(self.points) - 1):
            lines.append(Line(self.points[i], self.points[i + 1]))
        return lines


class Fence:
    #Represent the fence around the map using points and lines
    def __init__(self, points):
        self.points = points

    def getLines(self):
        lines = list()
        for i in range(0, len(self.points) - 1):
            lines.append(Line(self.points[i], self.points[i + 1]))
        return lines


class Line:
    def __init__(self, point1, point2):
        self.point1 = point1
        self.point2 = point2

    def angle(self):
        if (self.point1.x == self.point2.x):
            return 90
        else:
            return atan((self.point2.y - self.point1.y) / (self.point2.x - self.point1.x))

    def slope(self):
        return tan(self.angle())

    def intercept(self):
        if self.slope() is None:
            return None
        return self.point1.y - self.slope() * self.point1.x

    def __str__(self):
        return "(" + str(self.point1.x) + "," + str(self.point1.y) + ")-(" + str(self.point2.x) + "," + str(
            self.point2.y) + ")"


def getCurrentPosition(distances, turns, map):

    particles = list()
    for i in range(NUM_PARTICLES):
        x = np.random.randint(0, MAP_WIDTH)
        y = np.random.randint(0, MAP_HEIGHT)
        orient = np.random.randint(0, 360)
        particles.append(Particle(Point(x, y), orient, map))
        msg = CsData()
        msg.type = "PROCESSING...."
        msg.x = i
        msg.y = 0
        publish(msg)

    for i in range(0,len(distances)):
        msg = CsData()
        msg.type = "PROCESSING..."
        msg.x = i
        msg.y = 0
        publish(msg)
        distance = distances[i]
        turn = turns[i]

        particleTotalWeight = 0
        for particle in particles:
            particle.angle += turn
            pdist = particle.distanceToLandmarkOrFence()
            if pdist is not None:
                particle.weight = weightGaussianKernel(distance, pdist)
                particleTotalWeight += particle.weight

        if particleTotalWeight == 0:
            particleTotalWeight = 1e-8

        for particle in particles:
            particle.weight /= particleTotalWeight

        particles = resampling(particles)

    return selectOne(particles)


def broadCastPosition(x,y):
    try:
        requests.get("https://csrobot.aibizglobal.com/positions?x=%s&y=%s" % (x,y))
    except:
        pass



def readStableDistance():
    ### A wrapper function to reduce error
    #  in reading ultrasonic measurement for a static robot
    prevRead = 10000
    while True:
        currentRead = get_distance_by_sensor()
        if abs(currentRead - prevRead) < 1:
            break
        prevRead = currentRead
        time.sleep(1)
    return currentRead

def get_distance_by_sensor():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)

    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    startTime = time.time()
    stopTime = time.time()

    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        startTime = time.time()

    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        stopTime = time.time()

    # time difference between start and arrival
    timeElapsed = stopTime - startTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (timeElapsed * 34300) / 2
    return distance

def publish(data):
    pub = rospy.Publisher('cs_server_msg', CsData)
    rate = rospy.Rate(10) # 10hz
    pub.publish(data)
    rate.sleep()

def processClientMsg(data):
    global distances
    global turns

    msgType = data.type
    if msgType == "TURN":
        turn = data.x #used x to store the angle
        distance = readStableDistance() + SENSOR_FROM_COG
        distances.append(distance)
        turns.append(turn)
        msg = CsData()
        msg.type = "ACK"
        msg.x = 0
        msg.y = 0
        publish(msg)

    elif msgType == "PREDICT":
        turn = data.x #used x to store the angle
        distance = readStableDistance() + SENSOR_FROM_COG
        distances.append(distance)
        turns.append(turn)
        particle = getCurrentPosition(distances,turns,map)
        point = particle.point
        broadCastPosition(point.x, point.y)
        msg = CsData()
        msg.type = "LOCATION"
        msg.x = point.x
        msg.y = point.y
        publish(msg)
        distances = []
        turns = []

def listener():
    rospy.init_node('cs_server', anonymous=True)
    rospy.Subscriber("cs_client_msg", CsData, processClientMsg)
    rospy.spin() #simply keeps python from exiting until this node is stopped


if __name__ == '__main__':

    landmark1 = Landmark([Point(20, 36), Point(40, 36), Point(40, 38), Point(20, 38), Point(20, 36)])
    landmark2 = Landmark([Point(80, 36), Point(90, 36), Point(90, 38), Point(80, 38), Point(80, 36)])
    landmark3 = Landmark([Point(20, 71), Point(30, 71), Point(30, 73), Point(20, 73), Point(20, 71)])
    landmark4 = Landmark([Point(70, 71), Point(90, 71), Point(90, 73), Point(70, 73), Point(70, 71)])

    landmarks = [landmark1,landmark2,landmark3,landmark4]

    fencePoints = [Point(0, 0), Point(120, 0), Point(120, 100), Point(0, 100), Point(0, 0)]
    fence = Fence(fencePoints)


    map = Map(landmarks, fence)

    distances = []
    turns = []

    listener()
