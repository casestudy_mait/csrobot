#!/usr/bin/env python

#####
# USING MCL TO DETECT CORRECT ROBOT MOVEMENT
# This is a program for ROS environment that is designed to 
# work alongside arduino code to correct robot movement as it moves within a map.
# #### MAIT CASE STUDY ####
####

import bisect
import time
import RPi.GPIO as GPIO
import rospy
from case_study.msg import CsData
from random import random
import requests

import math
import numpy as np

SENSOR_FROM_COG = 12 #Distance from sensor to center of gravity is 12 cm

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24

#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

NUM_PARTICLES = 50
NUM_ITERATIONS = 3
R = 2


def weightGaussianKernel(robotDistance, particleDistance, std=0.7):
    euclideanDistance = np.linalg.norm(np.asarray(robotDistance) - np.asarray(particleDistance))
    # print(euclideanDistance,"eu",robotDistance,particleDistance,1 / np.sqrt(2 * np.pi) * np.exp(-euclideanDistance ** 2 /  (2 * std)))
    return 1 / np.sqrt(2 * np.pi) * np.exp(-euclideanDistance ** 2 / (2 * std))


def resampling(particles):
    distribution = WeightedDistribution(particles)
    newParticles = list()

    for i in range(len(particles)):
        particle = distribution.random_select()
        if particle is None:
            r = R * math.sqrt(random())
            theta = random() * 2 * math.pi
            x = particles[0].x + r * math.cos(theta) # use first particle as circle center
            y = particles[0].x + r * math.sin(theta)
            newParticles.append(Particle(Point(x, y)))

        else:
            newParticles.append(Particle(particle.point, particle.weight))

    return newParticles


def selectOne(particles):
    distribution = WeightedDistribution(particles)

    return distribution.random_select()


class WeightedDistribution:

    def __init__(self, particles):

        accum = 0.0
        self.particles = particles
        self.distribution = list()
        for particle in self.particles:
            accum += particle.weight
            self.distribution.append(accum)

    def random_select(self):

        try:
            return self.particles[bisect.bisect_left(self.distribution, np.random.uniform(0, 1))]
        except IndexError:
            # When all particles have weights zero
            return self.particles[0]


class Particle:
     # The MCL particle with point, angle and weight
    def __init__(self, point, weight=0):
        self.point = point
        self.weight = weight

    def __str__(self):
        return str(self.point.x) + "," + str(self.point.y)


class Point:
    # Particle point with x and y position in the map
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def distanceToPoint(self, point):
        return math.hypot(self.x - point.x, self.y - point.y)

    def __str__(self):
        return "x: " + str(self.x) + ", y: " + str(self.y)


def readStableDistance():
    ### A wrapper function to reduce error
    #  in reading ultrasonic measurement for a static robot

    prevRead = 10000
    while True:
        currentRead = get_distance_by_sensor()
        if abs(currentRead - prevRead) < 1:
            break
        prevRead = currentRead
        time.sleep(1)
    return currentRead

def get_distance_by_sensor():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)

    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    startTime = time.time()
    stopTime = time.time()

    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        startTime = time.time()

    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        stopTime = time.time()

    # time difference between start and arrival
    timeElapsed = stopTime - startTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (timeElapsed * 34300) / 2
    return distance


def broadCastPosition(x,y):
    try:
        requests.get("https://csrobot.aibizglobal.com/positions?x=%s&y=%s" % (x,y))
    except:
        pass

def predictPosition(initialDistance, initialPoint, newDistance, particles):
    particleTotalWeight = 0
    for particle in particles:
        pdist = initialDistance - particle.point.distanceToPoint(initialPoint)
        if pdist is not None:
            particle.weight = weightGaussianKernel(newDistance, pdist)
            particleTotalWeight += particle.weight

    if particleTotalWeight == 0:
        particleTotalWeight = 1e-8

    for particle in particles:
        particle.weight /= particleTotalWeight

    particles = resampling(particles)
    particle = selectOne(particles)
    return particle


def publish(data):
    pub = rospy.Publisher('cs_server_msg', CsData)
    rate = rospy.Rate(10) # 10hz
    pub.publish(data)
    rate.sleep()

def processClientMsg(data):
    global initPosition
    global initDistance

    msgType = data.type
    if msgType == "READY":
        initPosition = Point(data.x,data.y)
        initDistance = readStableDistance() + SENSOR_FROM_COG
        broadCastPosition(initPosition.x, initPosition.y)
        msg = CsData()
        msg.type = "MOVE"
        msg.x = 0 #not used
        msg.y = 0 #not used, added for data validity
        publish(msg)

    elif msgType == "LOCATION":
        point = Point(data.x,data.y)
        broadCastPosition(point.x, point.y) #UPDATE SERVER

    elif msgType == "PREDICT":
        particlesCenter = Point(data.x,data.y)
        broadCastPosition(particlesCenter.x, particlesCenter.y)
        particles = list()
        for i in range(NUM_PARTICLES):
            r = R * math.sqrt(random())
            theta = random() * 2 * math.pi
            x = particlesCenter.x + r * math.cos(theta)
            y = particlesCenter.y + r * math.sin(theta)
            particles.append(Particle(Point(x, y)))

        newDistance = readStableDistance() + SENSOR_FROM_COG
        particle = predictPosition(initDistance,initPosition, newDistance, particles)
        msg = CsData()
        msg.type = "CORRECT"
        msg.x = particle.point.x
        msg.y = particle.point.y
        publish(msg)

def listener():
    rospy.init_node('cs_server', anonymous=True)
    rospy.Subscriber("cs_client_msg", CsData, processClientMsg)
    rospy.spin() #simply keeps python from exiting until this node is stopped

if __name__ == '__main__':
    listener()
