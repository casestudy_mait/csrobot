/*
* ROBOT DETECTING ITS LOCATION IN A MAP AFTER TURNING
* This is a program for arduino microcontroller that is designed to 
* to work together with a python code using MCL to detect robot position and pos
* after the robot makes a number of turns to sense the environment.
* The communication environment is ROS (Robotic Operating System).
* Distance: (change in angle)*(wheel perimeter)/360
* #### MAIT CASE STUDY ####
*/

#include <Servo.h>
#include <ros.h>
#include <case_study/CsData.h>

#define _AVR_ATmega168_ 1

Servo myservo1;
Servo myservo2;

int pinFeedback1 = 9 ;
int pinFeedback2 = 10 ;

int pinServo1 = 5 ;
int pinServo2 = 6 ;

float angle1, angle2, turnAngle, startAngle;
int theta = 0, thetaP1 = 0, theta2 = 0, thetaP2 = 0;
unsigned long tHigh1 = 0, tLow1 = 0, tHigh2 = 0, tLow2 = 0;
float dc;
float dc2;
int tCycle1;
int tCycle2;
int rotAngle = 88;
int stopAngle = 90;
int turns1 = 0;
int turns2 = 0;
String robotStatus = "TURN";
bool stopMovement = false;
bool start_moving = false;
const float Pi = 3.14159;
const float wheelRadius = 3.2;
float wheelPerimeter = 2 * Pi * wheelRadius;
int Kp = 1;
int unitsFC = 360;
float dcMin = 0.029; //29 duty cycle
float dcMax = 0.971; //971 duty cycle
float dutyScale = 1.0;
float q2min = unitsFC / 4.0;
float q3max = q2min * 3.0;
float angleToTurn;
int turnCount = 0;

typedef  ros::NodeHandle_<ArduinoHardware, 1, 1, 256, 256> NodeHandle;
NodeHandle nh;

case_study::CsData csData;
ros::Publisher pub("cs_client_msg", &csData);


void processServerMsg(const case_study::CsData& data) {

  if (robotStatus == "WAITING" && strcmp((char*)data.type, "ACK") == 0) {
    robotStatus = "TURN";
  }
}

ros::Subscriber<case_study::CsData> sub("cs_server_msg", &processServerMsg);

void updateStatus(unsigned long& tHigh, int& tCycle, float& angle, int& thetaP, int& turns) {

  dc = (dutyScale * tHigh) / tCycle;//
  theta = ((dc - dcMin) * unitsFC) / (dcMax - dcMin); // full circle ,(dcMax - dcMin+1);

  if (theta < 0) theta = 0;
  else if (theta > (unitsFC - 1)) theta = unitsFC - 1;

  if ((theta < q2min) && (thetaP > q3max)) // If 4th to 1st quadrant
    turns++; // Increment turns count
  else if ((thetaP < q2min) && (theta > q3max)) // If in 1st to 4th quadrant
    turns --;// Decrement

  if (turns >= 0)
    angle = (turns * unitsFC) + theta;
  else if (turns1 < 0)
    angle = ((turns + 1) * unitsFC) - (unitsFC - theta);


  thetaP = theta;

}

int toRobotTurnAngle(int angle) {
  return round(150 * angle / 90);
}

int toReverseTurnAngle(int turnAngle) {
  return round(90*turnAngle / 150);
}

void updateServo1Angle(float& angle) {
  while (1) {
    tHigh1 = pulseIn(pinFeedback1, 1);// Measure high pulse
    tLow1 = pulseIn(pinFeedback1, 0);// Measure low pulse

    tCycle1 = tHigh1 + tLow1;// Calculate cycle time
    if ((tCycle1 > 1000) && (tCycle1 < 1200)) break; // Cycle time valid? Break!
  }

  updateStatus(tHigh1, tCycle1, angle, thetaP1, turns1);
}


void stopServo() {
  myservo1.writeMicroseconds(1500);
  myservo2.writeMicroseconds(1500);
}

void turnLeft() {
  myservo1.write(88);
  myservo2.write(88);
}

void setup() {

  pinMode(pinFeedback1, INPUT);
  pinMode(pinFeedback2, INPUT);
  myservo1.attach(pinServo1);
  myservo2.attach(pinServo2);
  angleToTurn = toRobotTurnAngle(45);
  updateServo1Angle(startAngle);
  //delay(1000);
  nh.initNode();
  nh.advertise(pub);
  nh.subscribe(sub);
}

void loop() {
  nh.spinOnce();
  if (robotStatus == "TURN") {
    turnLeft();
    delay(100);
    stopServo();
    delay(100);
    updateServo1Angle(turnAngle);
    if (angleToTurn - abs(turnAngle - startAngle) <= 3) {
      turnCount = turnCount + 1;
      startAngle = turnAngle;
      robotStatus = "WAITING";
      csData.type = turnCount == 8 ? "PREDICT" : "TURN";
      csData.x = toReverseTurnAngle(angleToTurn - abs(turnAngle - startAngle));
      csData.y = 0;
      pub.publish( &csData );
      nh.spinOnce();

      if (turnCount == 8) {
        robotStatus = "KEEP_SPINNING";
      }

      delay(1000);

    }

  }

}