/*
* THE REACTIVE ROBOT
* A robot that moves and tries to avoid obstacles
* Speed: distance/time
* #### MAIT CASE STUDY ####
*/

#include <Servo.h>

Servo servo1;
Servo servo2;
Servo sensorServo;
int servo1Pin = 9;
int servo2Pin = 6;
int servo3Pin = 4;
int feedbackPin = A0;
int feedbackValue;
int posAngle = 90;
int pos = 0;
int leftCm = 0;
int rightCm = 0;
int cm = 0;

void setup() {
   servo1.attach(servo1Pin);
   servo2.attach(servo2Pin);
   sensorServo.attach(servo3Pin);
   Serial.begin(9600);
   sensorServo.write(45);  
  
}

long readUltrasonicDistance(int triggerPin, int echoPin)
{
  pinMode(triggerPin, OUTPUT);  // Clear the trigger
  digitalWrite(triggerPin, LOW);
  delayMicroseconds(2);
  // Sets the trigger pin to HIGH state for 10 microseconds
  digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW);
  pinMode(echoPin, INPUT);
  // Reads the echo pin, and returns the sound wave travel time in microseconds
  return pulseIn(echoPin, HIGH);
}

void loop() {
   // measure the ping time in cm
    cm = 0.01723 * readUltrasonicDistance(12, 12);

    Serial.print(cm);
    Serial.println("cm");
  
    if(cm > 180){
    posAngle = 180;
    }
    else if(cm < 40){
      posAngle = 90;
      servo1.write(posAngle);
      servo2.write(posAngle);

     sensorServo.write(90);             
     delay(15); 
     rightCm = 0.01723 * readUltrasonicDistance(12, 12);
     sensorServo.write(0);            
     leftCm = 0.01723 * readUltrasonicDistance(12, 12);
     delay(15); 

     if(rightCm > leftCm){
      servo2.write(70);
     }
     else{
     servo1.write(70);
     }
     sensorServo.write(45);  
     delay(500);
     // servo1.write(90);
    }
    else{
    posAngle = map(cm, 0, 180, 90, 180);
    }
   
   servo1.write(posAngle);
   servo2.write(180 - posAngle);
   
   Serial.print("Servo1 Pos: ");
   Serial.println(servo1.read());
   Serial.print("Servo2 Pos: ");
   Serial.println(servo2.read());
  // feedbackValue = analogRead(feedbackPin);

   delay(100);
}