/*
* ROBOT MOVING IN ACROSSS POINTS IN A MAP WITH ODOMETRY + SENSOR FUSION
* This is a program for arduino microcontroller that is designed to 
* to move a 3 wheel robot within a 2d map and make a stop to specifiy positions in the map
* using odometry information and sensor fusion and particle filter localization.
* The communication environment is ROS (Robotic Operating System).
* Distance: (change in angle)*(wheel perimeter)/360
* #### MAIT CASE STUDY ####
*/

#include <Servo.h>
#include <ros.h>
#include <case_study/CsData.h>

#define _AVR_ATmega168_ 1

Servo myservo1;
Servo myservo2;

int pinFeedback1 = 9 ;
int pinFeedback2 = 10 ;

int pinServo1 = 5 ;
int pinServo2 = 6 ;

float angle1, angle2, turnAngle, startAngle;
int theta = 0, thetaP1 = 0, theta2 = 0, thetaP2 = 0;
unsigned long tHigh1 = 0, tLow1 = 0, tHigh2 = 0, tLow2 = 0;
float dc;
float dc2;
int tCycle1;
int tCycle2;
int rotAngle = 88;
int stopAngle = 90;
int turns1 = 0;
int turns2 = 0;
String robotStatus = "READY";
bool stopMovement = false;
bool start_moving = false;
const float Pi = 3.14159;
const float wheelRadius = 3.2;
float wheelPerimeter = 2 * Pi * wheelRadius;
int Kp = 1;
int unitsFC = 360;
float dcMin = 0.029; //29 duty cycle
float dcMax = 0.971; //971 duty cycle
float dutyScale = 1.0;
float q2min = unitsFC / 4.0;
float q3max = q2min * 3.0;
bool turn = false;
int turnDelay;
int maxTurnAngle = 310;
int minTurnAngle = 300;
int radDegFactor = 57.2958;
int p1[] = {15, 15};
int p2[] = {105, 15};
int p3[] = {105, 55};
int p4[] = {60, 55};
int p5[] = {60, 85};
int p6[] = {105, 85};
int p7[] = {15, 85};
int p8[] = {60, 55};
int p9[] = {60, 15};
int p10[] = {15, 15};
int positions[10][2];
int previousPoint[2];
int presentPoint[2];
int nextPoint[2];
float distanceToMove;
float angleToTurn;
int currentPosition = 0;
int startSensorReading;
int currentSensorReading;
int robotPositionX;
int robotPositionY;
String whichSensor;

typedef  ros::NodeHandle_<ArduinoHardware, 1, 1, 256, 256> NodeHandle;
NodeHandle nh;

case_study::CsData csData;
ros::Publisher pub("cs_client_msg", &csData);


String getRobotDirection() {

  int px = presentPoint[0];
  int py = presentPoint[1];

  int nx = nextPoint[0];
  int ny = nextPoint[1];

  String direction = "XF";

  if (ny == py && nx != px) {
    direction = nx > px ? "XF" : "XB";
  } else if (nx == px && ny != py) {
    direction = ny > py ? "YU" : "YD";
  }

  return direction;
}

void processServerMsg(const case_study::CsData& data) {

  if (robotStatus == "READY" && strcmp((char*)data.type, "MOVE") == 0) {
    int rightReading = readStableDistance("RIGHT");
    int leftReading = readStableDistance("LEFT");
    startSensorReading = leftReading < rightReading ? leftReading : rightReading;
    whichSensor = leftReading < rightReading ? "LEFT" : "RIGHT";
    robotStatus = "MOVE";
  }
  else if (robotStatus == "PREDICT" && strcmp((char*)data.type, "CORRECT") == 0) {
    int cx = data.x;
    int cy = data.y;

    String direction = getRobotDirection();

    if (direction == "XF") { //Moving forward in the x direction
      distanceToMove = robotPositionX > cx ? distanceToMove - (robotPositionX - cx) : distanceToMove + (cx - robotPositionX);
    }
    else if (direction == "XB") { //Moving backward in the x direction
      distanceToMove = robotPositionX > cx ? distanceToMove + (robotPositionX - cx) : distanceToMove - (cx - robotPositionX);
    }
    else if (direction == "YU") { //Moving upwards in the y direction
      distanceToMove = robotPositionY > cy ? distanceToMove - (robotPositionY - cy) : distanceToMove + (cy - robotPositionY);
    }
    else if (direction == "YD") { //Moving downwards in the y direction
      distanceToMove = robotPositionY > cy ? distanceToMove + (robotPositionY - cy) : distanceToMove - (cy - robotPositionY);
    }

    robotStatus = "MOVE";
  }

}

ros::Subscriber<case_study::CsData> sub("cs_server_msg", &processServerMsg);

void updateStatus(unsigned long& tHigh, int& tCycle, float& angle, int& thetaP, int& turns) {

  dc = (dutyScale * tHigh) / tCycle;//
  theta = ((dc - dcMin) * unitsFC) / (dcMax - dcMin); // full circle ,(dcMax - dcMin+1);

  if (theta < 0) theta = 0;
  else if (theta > (unitsFC - 1)) theta = unitsFC - 1;

  if ((theta < q2min) && (thetaP > q3max)) // If 4th to 1st quadrant
    turns++; // Increment turns count
  else if ((thetaP < q2min) && (theta > q3max)) // If in 1st to 4th quadrant
    turns --;// Decrement

  if (turns >= 0)
    angle = (turns * unitsFC) + theta;
  else if (turns1 < 0)
    angle = ((turns + 1) * unitsFC) - (unitsFC - theta);


  thetaP = theta;

}

void updateServo1Angle(float& angle) {
  while (1) {
    tHigh1 = pulseIn(pinFeedback1, 1);// Measure high pulse
    tLow1 = pulseIn(pinFeedback1, 0);// Measure low pulse

    tCycle1 = tHigh1 + tLow1;// Calculate cycle time
    if ((tCycle1 > 1000) && (tCycle1 < 1200)) break; // Cycle time valid? Break!
  }

 // Use the information to update the servo angle and turns
  updateStatus(tHigh1, tCycle1, angle, thetaP1, turns1);
}

void updateServo2Angle(float& angle) {
  while (1) {
    tHigh2 = pulseIn(pinFeedback2, 1);// Measure high pulse
    tLow2 = pulseIn(pinFeedback2, 0);// Measure low pulse

    tCycle2 = tHigh2 + tLow2;// Calculate cycle time
    if ((tCycle2 > 1000) && (tCycle2 < 1200)) break; // Cycle time valid? Break!
  }
 // Use the information to update the servo angle and turns
  updateStatus(tHigh2, tCycle2, angle, thetaP2, turns2);
}

float getDistance(int presentP[], int nextP[]) {

  float deltaX = nextP[0] - presentP[0];
  float deltaY = nextP[1] - presentP[1];

  float distanceSquare = pow(deltaX, 2) + pow(deltaY, 2);
  return sqrt(distanceSquare);

}

int toRobotLeftTurnAngle(int angle) {
  int factor = angle < 145 ? 180 : 165;
  return round(factor * angle / 90);
}

int toRobotRightTurnAngle(int angle) {
  return round(210 * angle / 90);
}
float getTurnAngle(int previousP[], int presentP[], int nextP[]) {

  float angle;
  int determinant;

  if (presentP[0] == previousP[0] && presentP[0] == nextP[0] || presentP[1] == previousP[1] && presentP[1] == nextP[1]) {
    determinant = 0;
    angle = 0;
    if ((previousP[1] > presentP[1] && nextP[1] > presentP[1] ) || (previousP[1] < presentP[1] && nextP[1] < presentP[1])) {
      angle = 180;
    }

    else if ((previousP[0] > presentP[0] && nextP[0] > presentP[0] ) || (previousP[0] < presentP[0] && nextP[0] < presentP[0])) {
      angle = 180;
    }

    return angle;
  }

  if  ((previousP[0] < presentP[0] and presentP[1] < nextP[1])
       or (previousP[1] < presentP[1] and presentP[0] > nextP[0])
       or  (previousP[0] > presentP[0] and  presentP[1] > nextP[1])
       or (previousP[1] > presentP[1] and presentP[0] < nextP[0])) {
    determinant = 1;
  }
  else {
    determinant = -1;
  }

  int a = getDistance(presentP, nextP);
  int b = getDistance(previousP, nextP);
  int c = getDistance(previousP, presentP);

  float angleB =  acos((pow(a, 2) + pow(c, 2) - pow(b, 2)) / (2 * a * c));
  angle =  round(angleB * radDegFactor);
  angle = 180 - angle;

  if (determinant < 0) {
    angle = -1 * angle;
  }

  return angle;
}

int getInterPointY(int presentP[], int nextP[], int angle, float distance) {
  int acuteAngle;
  if (angle < 90) {
    acuteAngle = 90 - angle;
  }
  else {
    acuteAngle = angle - 90;
  }

  float adj = cos(acuteAngle / radDegFactor) * distance;

  if (nextP[1] > presentP[1]) {
    return round(presentP[1] + adj);
  }
  else {
    return round(presentP[1] - adj);
  }
}


void stopServo() {
  myservo1.writeMicroseconds(1500);
  myservo2.writeMicroseconds(1500);
}

void turnLeft() {
  myservo1.write(88);
  myservo2.write(88);
}

void turnRight() {
  myservo1.write(96);
  myservo2.write(96);
}

void correctRight() {
  myservo1.write(89);
  myservo2.write(96);
}

void correctLeft() {
  myservo1.write(86);
  myservo2.write(96);
}

void moveServo() {
  myservo1.write(87);
  myservo2.write(96);
}

void turnBack() {
  myservo1.write(96);
  myservo2.write(87);
}

void stopServo1() {
  myservo1.write(1500);
}

void advanceServo1() {
  myservo1.write(86);
}

void reduceServo1() {
  myservo1.write(89);
}

long readUltrasonicDistance(int triggerPin, int echoPin)
{

  pinMode(triggerPin, OUTPUT);  // Clear the trigger
  digitalWrite(triggerPin, LOW);
  delayMicroseconds(2);
  // Sets the trigger pin to HIGH state for 10 microseconds
  digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW);
  pinMode(echoPin, INPUT);
  // Reads the echo pin, and returns the sound wave travel time in microseconds
  long duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  int distance = duration * 0.034 / 2;
  return distance;
}

int readStableDistance(String which) {
  int prevRead = 10000;
  int currentRead;
  while (true) {
    currentRead = (which == "RIGHT") ? readUltrasonicDistance(11,12) : readUltrasonicDistance(8,13);
    if (abs(currentRead - prevRead) < 1) {
      break;
    }
    prevRead = currentRead;
    delay(100);
  }
  return currentRead;
}

int getRobotXPosition(float distanceMoved, int fromPoint[], int toPoint[])
{
  int v[] = {toPoint[0] - fromPoint[0], toPoint[1] - fromPoint[1]};
  float mag =  sqrt(pow(v[0], 2) + pow(v[1], 2));
  float vNorm[] = {v[0] / mag, v[1] / mag};

  int x = round(fromPoint[0] + distanceMoved * vNorm[0]);
  return x;
}

int getRobotYPosition(float distanceMoved, int fromPoint[], int toPoint[])
{
  int v[] = {toPoint[0] - fromPoint[0], toPoint[1] - fromPoint[1]};
  float mag =  sqrt(pow(v[0], 2) + pow(v[1], 2));
  float vNorm[] = {v[0] / mag, v[1] / mag};

  int y = round(fromPoint[1] + distanceMoved * vNorm[1]);
  return y;
}

void setup() {

  for (int i = 0; i < 2; i++) {
    positions[0][i] = p1[i];
    positions[1][i] = p2[i];
    positions[2][i] = p3[i];
    positions[3][i] = p4[i];
    positions[4][i] = p5[i];
    positions[5][i] = p6[i];
    positions[6][i] = p7[i];
    positions[7][i] = p8[i];
    positions[8][i] = p9[i];
    positions[9][i] = p10[i];
    previousPoint[i] = positions[0][i];
    presentPoint[i] = positions[0][i];
    nextPoint[i] = positions[1][i];
  }

  pinMode(pinFeedback1, INPUT);
  pinMode(pinFeedback2, INPUT);
  myservo1.attach(pinServo1);
  myservo2.attach(pinServo2);
  delay(7500);
  nh.initNode();
  nh.advertise(pub);
  nh.subscribe(sub);
}


void loop() {
  nh.spinOnce();
  if (robotStatus == "READY") {
    distanceToMove = getDistance(presentPoint, nextPoint);
    csData.type = "READY";
    csData.x = presentPoint[0];
    csData.y = presentPoint[1];
    pub.publish( &csData );
    nh.spinOnce();
    delay(500);
  }
  else if (robotStatus == "MOVE") {
    int tCycle1 = 0;
    int tCycle2 = 0;
    currentSensorReading = readStableDistance(whichSensor);
    if (currentSensorReading > startSensorReading) {
      
      whichSensor == "RIGHT" ? correctRight() : correctLeft();
    }
    else if (startSensorReading > currentSensorReading) {
       whichSensor == "RIGHT" ? correctLeft() : correctRight();
    }
    else {
      moveServo();
    }

    delay(350);
    stopServo();
    updateServo1Angle(angle1);
    updateServo2Angle(angle2);

    float distanceCovered = (abs(angle1 - startAngle) / 360) * wheelPerimeter;

    if (distanceCovered >= distanceToMove) {
      csData.type = "LOCATION";
      csData.x = nextPoint[0];;
      csData.y = nextPoint[1];;
      pub.publish( &csData );
      nh.spinOnce();
      delay(500);
      turnAngle = angle1;
      currentPosition = currentPosition + 1;
      if (currentPosition == 9) {
        while (1) {
          delay(200000000);
        }
      }
      for (int i = 0; i < 2; i++) {
        previousPoint[i] = presentPoint[i];
        presentPoint[i] = nextPoint[i];
        nextPoint[i] = positions[currentPosition + 1][i];
      }

      float ta = getTurnAngle(previousPoint, presentPoint, nextPoint);
      distanceToMove = getDistance(presentPoint, nextPoint);
      if (abs(abs(ta) - 90) > 10 && abs(abs(ta) - 180) > 10 ) {
        int nextY = getInterPointY(presentPoint, nextPoint, ta, distanceToMove);
        nextPoint[0] = presentPoint[0];
        nextPoint[1] = nextY;
        ta = getTurnAngle(previousPoint, presentPoint, nextPoint);
        distanceToMove = getDistance(presentPoint, nextPoint);
        currentPosition = currentPosition - 1;
      }

    if(ta>=0){
         angleToTurn = toRobotLeftTurnAngle(ta);
        }
        else{
          angleToTurn = toRobotRightTurnAngle(ta);
        
          }

      robotStatus = "TURN";
    }

    else if (round(distanceCovered) % 10 == 0 && distanceCovered < distanceToMove - 3) {
      robotStatus = "PREDICT";

      robotPositionX = getRobotXPosition(distanceCovered, presentPoint, nextPoint);
      robotPositionY = getRobotYPosition(distanceCovered, presentPoint, nextPoint);
      csData.type = "PREDICT";
      csData.x = robotPositionX;
      csData.y = robotPositionY;
      pub.publish( &csData );
      nh.spinOnce();
      delay(500);
    }


  }
  else if (robotStatus == "TURN") {

    if (angleToTurn < 0) {
      turnRight();
    }
    else {
      turnLeft();
    }
    delay(100);
    stopServo();
    delay(100);
    updateServo1Angle(turnAngle);
    if (abs(angleToTurn) - abs(turnAngle - angle1) <= 3) {
      startAngle = turnAngle;
      robotStatus = "READY";
      distanceToMove = getDistance(presentPoint, nextPoint);
      csData.type = "READY";
      csData.x = presentPoint[0];
      csData.y = presentPoint[1];
      pub.publish( &csData );
      nh.spinOnce();

      delay(500);

    }

  }

}