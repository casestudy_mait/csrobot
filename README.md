Case Study: Robot Control
===============
This repository contains all the codes used in the research and development of Robot Control Algorithm for Case Studies.

__Project Authors:__ Christian C. Ndukwe, Larissa Melo


Folders
----------
__ros:__ The ros folder contains source codes and configuration assoicated with robotic operating system (ros).

__arudino:__ contians c codes for arduino micro controller

__python:__ contians python code used for MCL and as ROS scripts

__web:__ contians php/websocket and canvas based application interface for monitoring the robot movement.

